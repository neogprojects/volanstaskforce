const passport = require("passport");
const localStrategy = require("passport-local");
const mongoose = require("mongoose")

var User = mongoose.model("User");

passport.use(
    new localStrategy((IngameName, Password, done) =>{
            User.findOne({IngameName: IngameName}, (err, user) =>{
                if (err)
                    return done(err);
                else if (!user){
                    return done(null, false, {message: "Dein Nutzername oder Passwort stimmen nicht überein"});
                }
                else if (!user.verifyPassword(Password)){
                    return done(null, false, {message: "wrong password"});
                }
                else{
                    console.log(user)
                    return done(null, user);
                }
            }
            )
        }
    )
)