const express = require("express");
const app = express();
const mongoose = require("mongoose");  
const Build = require("./models/build-blog.js")
const User = require("./models/user-blog.js");
const rtsIndex = require("./routes/index.router");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
require("./config/passportConfig");
require("./config/config");

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(passport.initialize());

//MongoDB URL // PW W1CrWD18Fvt5xez8
let url = "mongodb+srv://Neo:W1CrWD18Fvt5xez8@taskforce.d8gz7.mongodb.net/TaskForce?retryWrites=true&w=majority"

//db connect
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true})
    .then((result)=>console.log("connected to db"))
    .catch((err)=> console.log(err))

//http freigabe wegen sicherheit und so
app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET,POST, PUT, DELETE");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
    next();
})

app.use(bodyParser.json());
app.use(cors());
app.use("/api", rtsIndex);

app.get("/addData", (req,res)=>{
    let build = new Build({
        DiscordID: 10,
        IngameName: "Neo",
        BuildName: "Heal",
        Waffe: "Fallen",
        WaffeTier: 8,
        WaffeSpecs: [100,100,100,100,100,100,100],
        Nebenhand: "keine",
        NebenhandTier: 8,
        NebenhandSpecs: [0,0,0,0,0],
        KName: "SöldnerKapuze",
        KTier: 8,
        KSpecs: [100,100,100,100,100,100,100,100],
        BName: "Kleriker Robe",
        BTier: 8,
        BSpecs: [100,100,100,100,100,100,100,100],
        SName: "Gelehrten Sandalen",
        STier: 8,
        SSpecs: [100,100,100,100,100,100,100,100],
        Umhang: "Lymhurst Umhang",
        UmhangTier: 8
    })
    build.save()
        .then((result)=>{
            res.send(result);
        })
        .catch((err)=>{
            console.log(err);
        })
});

app.get("/allData", (req,res)=>{
    User.find()
        .then((result)=>{
            res.send(result);
        })
        .catch((err) =>{
            console.log(err);
        })
});

app.get("/singleData/:id", (req,res)=>{
    let id = req.params.id;
    console.log(id);
    User.findById(id)
        .then((result)=>{
            res.send(result)
        })
        .catch((err)=>{
            console.log(err);
        })
})

app.post("/reciveUser",  (req,res)=>{
    console.log(req.body);

    let user = new User({
        DiscordID: req.body.DiscordID,
        IngameName: req.body.IngameName,
        Password: req.body.Password
    })
});

app.post("/reciveData", (req,res)=>{
    console.log(req.body);

    let build = new Build({
        DiscordID: req.body.DiscordID,
        IngameName: req.body.IngameName,
        BuildName: req.body.BuildName,
        Waffe: req.body.Waffe,
        WaffeTier: req.body.WaffeTier,
        WaffeSpecs: req.body.WaffeSpecs,
        Nebenhand: req.body.Nebenhand,
        NebenhandTier: req.body.NebenhandTier,
        NebenhandSpecs: req.body.NebenhandSpecs,
        KName: req.body.KName,
        KTier: req.body.KTier,
        KSpecs: req.body.KSpecs,
        BName: req.body.BName,
        BTier: req.body.BTier,
        BSpecs: req.body.BSpecs,
        SName: req.body.SName,
        STier: req.body.STier,
        SSpecs: req.body.SSpecs,
        Umhang: req.body.Umhang,
        UmhangTier: req.body.UmhangTier 
    })

    build.save()
        .then((result)=>{
            res.send(result);
            console.log("klappt");
        })
        .catch((err)=>{
            console.log(err);
        })

    res.status(201).send("Daten Erhalten")
})

app.listen(3100, (req,res)=>{
    console.log("Server listening on port 3100");
});