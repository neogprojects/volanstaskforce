const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken");
require("../config/config");

const JWT_SECRET = "üeoi§56m_3im6785 RUJHZI";

const blogSchema = new Schema({
    DiscordID: {
        type: Number,
        required: "DiscordID darf nicht leer sein"
    },
    IngameName: {
        type: String,
        required: "IngameName darf nicht leer sein",
        unique: true
    },
    Password:{
        type: String,
        required: "Password darf nicht leer sein",
        minlength: [8,"Das Passwort muss mindestens 8 stellen lang sein."]
    }
});

blogSchema.methods.verifyPassword = function(password) {
    return bcrypt.compare(password, this.Password);
}

blogSchema.methods.generateJwt = function(){
    return jwt.sign({_id: this._id},
        process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXP
        });
}

const User = mongoose.model("User", blogSchema);

module.exports = User;