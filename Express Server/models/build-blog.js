const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const blogSchema = new Schema({//DiscordID, IngameName, GearName usw., Umhang+T, Waffe+T+Specs, Nebenhand+T+Specs, Rüstung+T+Specs K Kopf, B Brust, S Schuhe,  
    DiscordID: {
        type: Number,
        required: true
    },
    IngameName: {
        type: String,
        required: true
    },
    BuildName:{
        type: String,
        required: true
    },
    Waffe:{
        type: String,
        required: true
    },
    WaffeTier:{
        type: Number,
        required: true
    },
    WaffeSpecs:{
        type: Array,
        required: true
    },
    Nebenhand:{
        type: String,
        required: true
    },
    NebenhandTier:{
        type: Number,
        required: true
    }, 
    NebenhandSpecs:{
        type: Array,
        required: true
    },
    KName:{
        type: String,
        required: true
    },
    KTier:{
        type: Number,
        required: true
    },
    KSpecs:{
        type: Array,
        required: true
    },
    BName:{
        type: String,
        required: true
    },
    BTier:{
        type: Number,
        required: true
    },
    BSpecs:{
        type: Array,
        required: true
    },
    SName:{
        type: String,
        required: true
    },
    STier:{
        type: Number,
        required: true
    },
    SSpecs:{
        type: Array,
        required: true
    },
    Umhang:{
        type: String,
        required:true
    },
    UmhangTier:{
        type: Number,
        required: true
    }
}, {timestamps: true});

const Build = mongoose.model("Build", blogSchema);

module.exports = Build;