const mongoose = require("mongoose");  
const bcrypt = require("bcryptjs");
const passport = require("passport");
const _ = require("lodash");

var User = mongoose.model("User");

module.exports.register = (req, res, next) =>{
    let user = new User();
    user.DiscordID = req.body.DiscordID;
    user.IngameName = req.body.IngameName;
    user.Password = req.body.Password;

    bcrypt.genSalt(10, (err, salt) =>{
        bcrypt.hash(user.Password, salt, (err, hash)=>{
            if(err){
                console.log(err);
            }
            user.Password = hash;
            user.save((err,doc)=> {
                if(!err){
                    res.send(doc);
                }else{
                    console.log(err);
                }
            })
        })
    });
}

module.exports.authenticate = (req,res,next) =>{
    passport.authenticate("local", (err, user, info)=>{
        if (err){
            return res.status(400).json(err);
        } 
        else if (user){
            console.log("User gefunden MASHALAA")
            return res.status(200).json({"token": user.generateJwt()})
        } 
        else return res.status(404).json(info)
    })(req,res);
}

module.exports.userProfile = (req, res, next) =>{
    User.findOne({ _id:req._id}, (err, user) =>{
        if(err)
            console.log(err);
            next();
        if(!user)
            return res.status(404).json({ status: false, message: "Nutzer wurde nicht gefunden"});
        else{
            return res.status(200).json({ status: true, User: _.pick(user,["DiscordID","IngameName"])}); 
        }
    })
}