import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private http: HttpClient) { }

  fetchData(): Observable<User>{
    this.http.get<any>("http://localhost:3100/getData").subscribe(
      data=> console.log(data)
    )
    return this.http.get<any>("http://localhost:3100/getData")
  }

  postUserData(Users:any,DiscordID:number,IngameName:String,BuildName:String,Waffe:String,WaffeTier:number,WaffeSpecs:Array<Number>,Nebenhand:String,NebenhandTier:Number,NebenhandSpecs:Array<Number>,KName:String,KTier:Number,KSpecs:Array<Number>,BName:String,BTier:Number,BSpecs:Array<Number>,SName:String,STier:Number,SSpecs:Array<Number>,Umhang:String,UmhangTier:Number){
    this.http.post("http://localhost:3100/reciveData",{
      DiscordID,
      IngameName,
      BuildName,
      Waffe,
      WaffeTier,
      WaffeSpecs,
      Nebenhand,
      NebenhandTier,
      NebenhandSpecs,
      KName,
      KTier,
      KSpecs,
      BName,
      BTier,
      BSpecs,
      SName,
      STier,
      SSpecs,
      Umhang,
      UmhangTier
    }).toPromise().then((data:any) => {
      Users = data;
    })
  }
}
