import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VolansNewsComponent } from './volans-news.component';

describe('VolansNewsComponent', () => {
  let component: VolansNewsComponent;
  let fixture: ComponentFixture<VolansNewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VolansNewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VolansNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
