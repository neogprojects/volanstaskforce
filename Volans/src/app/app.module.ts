import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserTableComponent } from './user-table/user-table.component';
import { NavbarComponent } from './navbar/navbar.component';
import { VolansNewsComponent } from './volans-news/volans-news.component';

//Anmeldung, Nach eigenen Builds Filtern, Filterfunktioin,
//Registrieren DiscordID, IngameName, Passwort
//Login IngameName Passwort

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    UserTableComponent,
    NavbarComponent,
    VolansNewsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
