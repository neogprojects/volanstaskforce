import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  constructor(private api:ApiServiceService, private http: HttpClient){}

  public Users:any = [];

  ngOnInit(){
    this.api.fetchData()
      .subscribe(data => this.Users = data)
  }

  isNotTwoHanded(Waffe:string){
    if (Waffe === "Blutklinge" || Waffe === "Arkanstab" || Waffe === "Ava- Eisstab" || Waffe === "Ava- Feuerstab" || Waffe === "Keule"){
      return false
    }else{
      return true
    }
  }

  createUser(Users:any,User:any){
    if (!this.isNotTwoHanded(User.Waffe)){
      this.api.postUserData(
        Users,
        User.DiscordID,
        User.IngameName,
        User.BuildName,
        User.Waffe,
        User.WaffeTier,
        User.WaffeSpecs,
        User.Nebenhand,
        User.NebenhandTier,
        User.NebenhandSpecs,
        User.KName,
        User.KTier,
        User.KSpecs,
        User.BName,
        User.BTier,
        User.BSpecs,
        User.SName,
        User.STier,
        User.SSpecs,
        User.Umhang,
        User.UmhangTier
        )
    }else{
      console.log(User);
      this.api.postUserData(
        Users,
        User.DiscordID,
        User.IngameName,
        User.BuildName,
        User.Waffe,
        User.WaffeTier,
        User.WaffeSpecs,
        "keine Nebenhand",
        0,
        [0, 0, 0, 0, 0],
        User.KName,
        User.KTier,
        User.KSpecs,
        User.BName,
        User.BTier,
        User.BSpecs,
        User.SName,
        User.STier,
        User.SSpecs,
        User.Umhang,
        User.UmhangTier
        )
    }

    User.DiscordID = null
    User.IngameName = ""
    User.BuildName = ""
    User.Waffe = ""
    User.WaffeTier = null
    User.WaffeSpecs = [null,null,null,null,null,null,null]
    User.Nebenhand = ""
    User.NebenhandTier = null
    User.NebenhandSpecs = [null,null,null,null,null]
    User.KName = ""
    User.KTier = null
    User.KSpecs = [null,null,null,null,null,null,null,null]
    User.BName = ""
    User.BTier = null
    User.BSpecs = [null,null,null,null,null,null,null,null]
    User.SName = ""
    User.STier = null
    User.SSpecs = [null,null,null,null,null,null,null,null]
    User.Umhang = ""
    User.UmhangTier = null
  }

  waffenSpec=[0,1,2,3,4,5,6];
  offHandSpecs=[0,1,2,3,4];
  RuestungSpecs=[0,1,2,3,4,5,6,7]; // Blutklinge, Arkanstab, Ava- Eisstab, Ava- Feuerstab
  waffenTier=[4,5,6,7,8];
  builds=["Tank - Initiator","Tank - FollowUp","Tank - Defensive","Heal - Holy","Support","Melee","Melee - Bruiser","Melee - Clapper","Ranged","Battlemount"];
  waffen=["Hainhüter","Energieformer","Camlann-Keule","Eiszapfenstab","Seelensense","Schwere Keule","Gefallener Stab","Großer Heiligenstab","Randalierender Stab","Wildstab","Okkulter Stab","Geheimnisvolle Sphäre","Arkanstab","Hellebarde","Zweischwerter","Bärentatzen","Belagerungsbogen","Blutklinge","Permafrostprisma","Schwefelstab","Großer Froststab","Verfluchter Schädel","Galatinepaar","Ava- Axt","Ava- Schwert","Ava- Eisstab","Ava- Feuerstab","Ava- Hammer","Ava- Bogen","Badon-Bogen","Keule"];
  nebenhand=["Nebelrufer","Pfahlwurzel"];
  helme=["Rechtssprecherhelm","Beschützerhelm","Attentäterkapuze","Ritterhelm","Soldatenhelm","Söldnerkapuze","Gelehrtengugel","Königliche Kapuze"];
  brust=["Beschützerrüstung","Rechtssprecherrüstung","Ritterrüstung","Dämonenrüstung","Klerikerobe","Magierrobe","Gelehrtenrobe","Soldatenrüstung","Attentäterjacke","Teufelsbratenjacke","Gespensterjacke"];
  schuhe=["Jägerschuhe","Gelehrtensandalen","Königliche Sandalen","Beschützerstiefel","Magiersandalen"];
  umhaenge=["Martlock-Umhang","Fort-Sterling-Umhang","Lymhurst-Umhang","Hüterumhang"];

  DefaultUser = {
    DiscordID : null,
    IngameName : "",
    BuildName : "",
    Waffe : "",
    WaffeTier : null,
    WaffeSpecs : [null,null,null,null,null,null,null],
    Nebenhand : "",
    NebenhandTier : null,
    NebenhandSpecs : [null,null,null,null,null],
    KName : "",
    KTier : null,
    KSpecs : [null,null,null,null,null,null,null,null],
    BName : "",
    BTier : null,
    BSpecs : [null,null,null,null,null,null,null,null],
    SName : "",
    STier : null,
    SSpecs : [null,null,null,null,null,null,null,null],
    Umhang : "",
    UmhangTier : null
}
User = {
  DiscordID : null,
  IngameName : "",
  BuildName : "",
  Waffe : "",
  WaffeTier : null,
  WaffeSpecs : [null,null,null,null,null,null,null],
  Nebenhand : "",
  NebenhandTier : null,
  NebenhandSpecs : [null,null,null,null,null],
  KName : "",
  KTier : null,
  KSpecs : [null,null,null,null,null,null,null,null],
  BName : "",
  BTier : null,
  BSpecs : [null,null,null,null,null,null,null,null],
  SName : "",
  STier : null,
  SSpecs : [null,null,null,null,null,null,null,null],
  Umhang : "",
  UmhangTier : null
}

}
