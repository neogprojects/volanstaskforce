import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../user';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

  constructor(private api:ApiServiceService, private http: HttpClient) { }

  public Users:any = [];

  ngOnInit(): void{
    this.api.fetchData()
      .subscribe(data => this.Users = data)
  }

  getAllSpecs(User:any){
    return  User.WaffeSpecs[0] + User.WaffeSpecs[1] + User.WaffeSpecs[2] + User.WaffeSpecs[3] + User.WaffeSpecs[4] + User.WaffeSpecs[5] + User.WaffeSpecs[6] +
            User.KSpecs[0] + User.KSpecs[1] + User.KSpecs[2] + User.KSpecs[3] + User.KSpecs[4] + User.KSpecs[5] + User.KSpecs[6] + User.KSpecs[7] +
            User.BSpecs[0] + User.BSpecs[1] + User.BSpecs[2] + User.BSpecs[3] + User.BSpecs[4] + User.BSpecs[5] + User.BSpecs[6] + User.BSpecs[7] +
            User.SSpecs[0] + User.SSpecs[1] + User.SSpecs[2] + User.SSpecs[3] + User.SSpecs[4] + User.SSpecs[5] + User.SSpecs[6] + User.SSpecs[7] 
  }
/*
  hilf:any;
  
    for (const j in Users) {
        if (this.getAllSpecs(Users[j]) < this.getAllSpecs(Users[parseInt(j)+1])){
          this.hilf = Users[j];
          Users[j] = Users[parseInt(j)+1];
          Users[parseInt(j)+1] = this.hilf;
        }
      }
  

  sortUsers(Users:Array<object>){
    for (const i of Users) {
      console.log(i)
    }
    console.log("sortiert");
  }

*/   

  waffenSpec=[0,1,2,3,4,5,6];
  offHandSpecs=[0,1,2,3,4];
  RuestungSpecs=[0,1,2,3,4,5,6,7];
  waffenTier=[4,5,6,7,8];
}
