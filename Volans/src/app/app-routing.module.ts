import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserFormComponent } from './user-form/user-form.component';
import { UserTableComponent } from './user-table/user-table.component';
import { VolansNewsComponent } from './volans-news/volans-news.component';

const routes: Routes = [
  {
    path: "UserForm",
    component: UserFormComponent
  },
  {
    path: "MitgliederTabelle",
    component: UserTableComponent
  },
  {
    path: "VolansNews",
    component: VolansNewsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
